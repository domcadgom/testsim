
mkdir -p res/genome
echo "Downloading genome info..."
wget -O res/genome/ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
echo "Unzipping genome info..."
yes n | gunzip -k res/genome/ecoli.fasta.gz
echo "Done previous work!"


echo "Running STAR index..."
mkdir -p res/genome/star_index
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fasta --genomeSAindexNbases 9
echo

echo "Running FastQC..."
mkdir -p out/fastqc
fastqc -o out/fastqc data/*.fastq.gz
echo "Finished FastQC!"




for sid in $(ls data/*.fastq.gz | cut -d"_" -f1 | cut -d/ -f2 | sort | uniq)
do

	echo "Running main script..."
	bash scripts/analyse_sample.sh $sid 



done

